from django.shortcuts import render

import joblib

from web.services import treatment_requests_data


def predict_energy(request):
    output_data = {}
    if request.method == "POST":
        input_data = request.POST.dict()
        input_data_valid = [treatment_requests_data(input_data)]

        if input_data_valid:
            model_for_energy = joblib.load("model_for_energy.pkl")
            model_for_impact_e = joblib.load("model_for_impact_e.pkl")
            output_data = {
                'energy': round(list(model_for_energy.predict(input_data_valid))[0], 4),
                'impact_e': round(list(model_for_impact_e.predict(input_data_valid))[0], 4)
            }

    return render(request, "index.html", {'data': output_data})
