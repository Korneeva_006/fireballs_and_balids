def treatment_requests_data(data: dict):
    data_list = list(data.values())
    data_list_num = convert_to_numeric(data_list[1::])

    return data_list_num


def convert_to_numeric(data: list):
    for i in range(len(data)):
        try:
            data[i] = float(data[i])
        except:
            return False

    return data
