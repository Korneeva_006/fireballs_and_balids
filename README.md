# fireballs_and_balids

анализ данных, взятых с [NASA OPEN API](https://ssd-api.jpl.nasa.gov/doc/fireball.html), по файрболам и болидам и 
предсказание полной энергии и энергии удара

### Документы
- [ТЗ](https://docs.google.com/document/d/1a0YHlJHs_8gmVQ1OfdNJ_KvKHX7I5z3sKVPJNI65dNA/edit?usp=sharing)
- [action-plan](https://docs.google.com/spreadsheets/d/1DNJy2_o983e59ixYskbMbbS8Y76qGaymKSWiffrByP4/edit?usp=sharing)
- [Макет сайта](https://www.figma.com/file/IrYpNBovyH6AIxxubr394k/Fireballs?type=design&node-id=0-1&t=SxPK3pouLxmEEDLo-0)
- [Обучение модели](https://colab.research.google.com/drive/1dQ-dWzY-QaoTL2CnR8MacieYRRi_SpF_?usp=sharing)
