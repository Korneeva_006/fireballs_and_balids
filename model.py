import json
from sklearn.model_selection import train_test_split
from sklearn import linear_model


def convert_to_float(data: [str]) -> [float]:
    for i in range(len(data)):
        for j in range(len(data[i])):
            try:
                data[i][j] = float(data[i][j])
            except ValueError:
                "Error occurred while converting to float"
    return data


def read_data_from_file(filename: str) -> {}:
    with open(filename, "r") as f:
        data = json.load(f)
    return data


if __name__ == '__main__':
    nasa_data = read_data_from_file("data.json")
    numeric_data = convert_to_float(nasa_data.get("data"))

    output_energy = [el[0] for el in numeric_data]
    output_impact_e = [el[1] for el in numeric_data]
    input_data = [el[2:] for el in numeric_data]

    X_train, X_test, y_train, y_test = train_test_split(
        input_data, output_energy, test_size=0.33, random_state=42
    )

    reg = linear_model.LinearRegression()
    reg.fit(X_train, y_train)
    print(reg.coef_)
