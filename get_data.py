import json

import requests


res = requests.get('https://ssd-api.jpl.nasa.gov/fireball.api?req-vel=True&vel-comp=True')

content = res.text
content = json.loads(content)
field = content['fields']
data = content['data']

new_data = {'field': field[1:3] + field[-5:], 'data': []}
for el in data:
    if el[-5]:
        new_data['data'].append(el[1:3] + el[-5:])

json_string = json.dumps(new_data)
with open('data.json', 'w') as outfile:
    outfile.write(json_string)
